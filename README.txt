Entity Visits tracks the users who viewed the entity. 

INSTALLATION

Proceed as with all modules in Drupal (see https://www.drupal.org/documentation/install/modules-themes/modules-7). 

CONFIGURATION

General settings can be configured at: /config/system/entity_visits

Permissions can be set here: /admin/people/permissions#module-entity_visits

USAGE

Here are some preliminary instructions for using this module:

 * Configure entity_visits to track the desired elements (/config/system/entity_visits)
 * (Optional) Enable the User Visits (User) view
 * (Optional) Create a new "Entity Visits"-type view with appropriate filters and fields. If, for example, you wanted to show an aggregate count of distinct visitors to pages within the last 2.5 hours, you could:
   - Add "Entity Visits: Visitor of Entity" field
   - Add "Entity Visits: Date" as "Filter criteria" field, set to "Is greater than or equal to" and "-2 hours -30 minutes"
   - Set the view's "Use aggregation" setting to "Yes"
   - Then in the "Aggregation settings" of the "Entity Visits: Visitor of Entity" field, set it  to use "Count DISTINCT"

Please consider adding documentation to this file for other use-cases.
